package io.swagger.api;

import io.swagger.annotations.*;
import io.swagger.model.LoginDTO;
import io.swagger.model.LoginResponseDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2017-03-21T15:56:52.008+01:00")

@Api(value = "login", description = "the login API")
public interface LoginApi {

    @ApiOperation(value = "", notes = "Bejelentkezés", response = LoginResponseDTO.class, tags = {"Login",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Sikeres regisztráció", response = LoginResponseDTO.class)})
    @RequestMapping(value = "/login",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.POST)
    ResponseEntity<LoginResponseDTO> login(
            @ApiParam(value = "") @RequestBody LoginDTO loginDTO
    );

}
