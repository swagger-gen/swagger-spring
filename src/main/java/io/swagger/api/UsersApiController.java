package io.swagger.api;

import io.swagger.model.UserDTO;
import io.swagger.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import java.util.List;


@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2017-03-21T15:56:52.008+01:00")

@Controller
public class UsersApiController implements UsersApi {

    public ResponseEntity<List<UserDTO>> getUsers() {
        // do some magic!
        UserService service = new UserService();
        return new ResponseEntity<List<UserDTO>>(service.getUsers(), HttpStatus.OK);
    }

}
