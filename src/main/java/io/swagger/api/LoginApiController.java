package io.swagger.api;

import io.swagger.annotations.ApiParam;
import io.swagger.model.LoginDTO;
import io.swagger.model.LoginResponseDTO;
import io.swagger.service.LoginService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;


@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2017-03-21T15:56:52.008+01:00")

@Controller
public class LoginApiController implements LoginApi {

    public ResponseEntity<LoginResponseDTO> login(@ApiParam(value = "") @RequestBody LoginDTO loginDTO) {
        LoginService service = new LoginService();
        return new ResponseEntity<LoginResponseDTO>(service.login(loginDTO), HttpStatus.OK);
    }

}
