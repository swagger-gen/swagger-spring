package io.swagger.service;

import io.swagger.model.UserDTO;

import java.util.ArrayList;
import java.util.List;

public class UserService {
    public List<UserDTO> getUsers() {
        List<UserDTO> users = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            UserDTO user = new UserDTO();
            user.setId(i);
            user.setName("Teszt Elek " + i);
            users.add(user);
        }

        return users;
    }
}
