# Swagger Spring Boot generated server

```
java -jar swagger-codegen-cli-2.2.1.jar generate -i swagger.json -l spring
```

Generálás utáni teendők (WildFly futtatáshoz):
* Az application osztályt a SpringBootServletInitializer-ből kell száramztatni 
```
public class Swagger2SpringBoot extends SpringBootServletInitializer {

    private static Class<Swagger2SpringBoot> applicationClass = Swagger2SpringBoot.class;

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(applicationClass);
    }
}
```
* pom-ban a függőségeket a követlkező módon kell kialakítani:
```
    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
            <exclusions>
                <exclusion>
                    <groupId>org.springframework.boot</groupId>
                    <artifactId>spring-boot-starter-tomcat</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>servlet-api</artifactId>
            <version>2.5</version>
            <scope>provided</scope>
        </dependency>
    </dependencies>
```
http://www.mastertheboss.com/jboss-frameworks/spring/spring-boot-hello-world-on-wildfly
* service osztályokban a logika megvalósítása


Előnyök:
* Válaszok is yaml alapján generálódnak le
```
@ApiOperation(value = "", notes = "", response = UserDTO.class, responseContainer = "List", tags = {"User",})
@ApiResponses(value = {
        @ApiResponse(code = 200, message = "user lista", response = UserDTO.class)})
@RequestMapping(value = "/users",
        produces = {"application/json"},
        consumes = {"application/json"},
        method = RequestMethod.GET)
ResponseEntity<List<UserDTO>> getUsers();
```

Hátrányok:
* kérés headerbe mindenképpen bele kell tenni a következő paramétert: Content-Type: application/json
* Spring Boot nem túl "nagy" egy rest megvalósításhoz?